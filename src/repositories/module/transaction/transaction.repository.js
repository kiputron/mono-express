import { ErrorHandler } from "../../../config/http";
import { UserTransactionModel } from "../../../database/models";

export default {
  check_transaction: async (id, user_id) => {
    const transaction = await UserTransactionModel.findOne({
      attributes: ["id", "name", "type", "amount", "date"],
      where: {
        id,
        user_id,
      },
    });

    if (!transaction)
      throw new ErrorHandler("Transaction not found", null, 404);

    return transaction;
  },
};
