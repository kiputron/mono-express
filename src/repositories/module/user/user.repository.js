import { ErrorHandler } from "../../../config/http";
import { UserModel } from "../../../database/models";

export default {
  email_unique: async email => {
    const data = await UserModel.findOne({
      where: { email },
      raw: true,
    });

    if (data)
      throw new ErrorHandler("Sorry, this email is already in use", null, 422);

    return true;
  },
};
