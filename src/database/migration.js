import { connectionCheck, db } from ".";
import logger from "../utils/logger";
import { UserModel, UserTransactionModel } from "./models";

const runMigrations = async () => {
  await connectionCheck();

  await UserModel.sync();
  await UserTransactionModel.sync();

  logger.info("Migrations have been executed successfully!");

  await db.close();
};

runMigrations();
