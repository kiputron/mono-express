import UserModel from "./module/user/user.model";
import UserTransactionModel from "./module/user/user-transaction.model";

UserModel.hasMany(UserTransactionModel, {
  foreignKey: "user_id",
  as: "transactions",
});

export { UserModel, UserTransactionModel };
