import sequelizePkg from "sequelize";
import { db } from "../../../index";
import { TransactionConstant } from "../../../../constant";

const { DataTypes } = sequelizePkg;

const Model = db.define(
  "user_transaction",
  {
    id: {
      type: DataTypes.BIGINT(20).UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    type: {
      type: DataTypes.ENUM(...TransactionConstant.ALL_TYPE),
      allowNull: true,
    },
    date: {
      type: DataTypes.DATE(),
      allowNull: true,
    },
    amount: {
      type: DataTypes.DECIMAL(20, 2),
      allowNull: false,
      defaultValue: "0.00",
    },
    user_id: {
      type: DataTypes.BIGINT(20).UNSIGNED,
      allowNull: false,
    },
  },
  {
    timestamps: true,
    paranoid: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
    deletedAt: "deleted_at",
  }
);

export default Model;
