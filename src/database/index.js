import { Sequelize } from "sequelize";

import logger from "../utils/logger";
import env from "../config/env";

const db = new Sequelize(env.DB.NAME, env.DB.USER, env.DB.PASSWORD, {
  host: env.DB.HOST,
  port: env.DB.PORT,
  dialect: "mysql",
  pool: {
    max: 256,
    min: 0,
    idle: 30000,
    acquire: 100000,
  },
  logging: false,
  dialectOptions: {
    dateStrings: true,
    typeCast: true,
  },
  timezone: "+07:00",
});

async function connectionCheck() {
  return db
    .authenticate()
    .then(() => logger.info("Connection Database Successfully!"))
    .catch(err => logger.error(err.original));
}

function dbTransaction() {
  const transaction = db.transaction();

  return transaction;
}

function offsetPagination(page, limit) {
  return page < 1 ? 0 : Number(page - 1) * Number(limit);
}

function pagination(data, current_page = 1, limit = 10) {
  const total_page = Math.ceil(data.count / Number(limit));
  const total_records = data.count;
  const records = data.rows;

  return {
    current_page: Number(current_page),
    total_page,
    total_records,
    records,
  };
}

export { db, connectionCheck, dbTransaction, offsetPagination, pagination };
