import express from "express";
import logger from "./utils/logger";
import env from "./config/env";
import { connectionCheck } from "./database";
import moment from "moment";
import router from "./routes";
import requestHandler from "./middleware/request-handler";
import bodyParser from "body-parser";
import ErrorHandlerMiddleware from "./middleware/error-handler.middleware";

const app = express();
const port = env.SERVER.PORT;

app.use(bodyParser.json({ limit: "50mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: false,
  })
);

app.use(requestHandler);

app.use("/api", router());

app.use((req, res) => {
  res.status(404).send({ message: "Not Found" });
});

app.use(ErrorHandlerMiddleware);

app.listen(port, () => {
  console.clear();
  logger.info(`Server started. Listening on port ${port}`);

  moment.tz.setDefault("Asia/Jakarta");

  connectionCheck();
});
