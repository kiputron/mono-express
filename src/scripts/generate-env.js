import fs from "fs";
import crypto from "crypto";
import logger from "../utils/logger";

const generateEnv = () => {
  const secret_key = crypto.randomBytes(32).toString("hex");

  const env_content = `
SERVER_PORT=5050

DB_HOST=127.0.0.1
DB_USER=root
DB_PASSWORD=
DB_NAME=mono
DB_PORT=3306

JWT_SECRET_KEY=${secret_key}
`;

  fs.writeFileSync(".env", env_content);

  logger.info(".env file generated successfully.");

  logger.info("Secret key generated and saved to .env file.");

  process.exit();
};

generateEnv();
