import dotenv from "dotenv";

dotenv.config();

const env = {
  SERVER: {
    PORT: process.env.SERVER_PORT,
  },
  DB: {
    HOST: process.env.DB_HOST,
    USER: process.env.DB_USER,
    PASSWORD: process.env.DB_PASSWORD,
    NAME: process.env.DB_NAME,
    PORT: process.env.DB_PORT,
  },
  JWT: {
    SECRET_KEY: process.env.JWT_SECRET_KEY,
  },
};

export default env;
