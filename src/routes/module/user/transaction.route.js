import express from "express";
import { UserTransactionController } from "../../../controller";
import { UserTransactionValidator, resultValidator } from "../../../validator";
const route = express.Router();

export default function UserTransactionRouter() {
  route.get("/", UserTransactionController.all);

  route.get("/wallet", UserTransactionController.wallet);

  route.get("/:id", UserTransactionController.detail);

  route.post(
    "/",
    UserTransactionValidator.create,
    resultValidator,
    UserTransactionController.create
  );

  route.put(
    "/:id",
    UserTransactionValidator.create,
    resultValidator,
    UserTransactionController.update
  );

  route.delete("/:id", UserTransactionController.delete);

  return route;
}
