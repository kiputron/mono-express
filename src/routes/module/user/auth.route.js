import express from "express";
import { UserAuthController } from "../../../controller";
import { UserAuthValidator, resultValidator } from "../../../validator";

const route = express.Router();

export default function UserAuthRouter() {
  route.post(
    "/register",
    UserAuthValidator.register,
    resultValidator,
    UserAuthController.register
  );

  route.post(
    "/login",
    UserAuthValidator.login,
    resultValidator,
    UserAuthController.login
  );

  return route;
}
