import express from "express";
import UserRouter from "./user.route";

const route = express.Router();

export default function router() {
  route.use("/ping", (req, res) => {
    res.send("Pong");
  });

  route.use(UserRouter());

  return route;
}
