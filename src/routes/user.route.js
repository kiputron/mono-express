import express from "express";

import { UserAuthRouter, UserTransactionRouter } from "./module";
import { AuthMiddleware } from "../middleware";

const route = express.Router();

export default function UserRouter() {
  route.use("/v1/auth", UserAuthRouter());

  route.use(
    "/v1/transaction",
    AuthMiddleware.user_guard,
    UserTransactionRouter()
  );

  return route;
}
