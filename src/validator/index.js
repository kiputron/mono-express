import { validationResult } from "express-validator";
import UserAuthValidator from "./module/user/auth.validator";
import UserTransactionValidator from "./module/user/transaction.validator";
import { ErrorHandler } from "../config/http";

function resultValidator(req, res, next) {
  const validated = validationResult(req);

  if (!validated.isEmpty())
    return next(new ErrorHandler("Invalid data", validated.errors, 422));

  return next();
}

export { resultValidator, UserAuthValidator, UserTransactionValidator };
