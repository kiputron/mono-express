import { check } from "express-validator";
import { UserRepository } from "../../../repositories";

const name = check("name")
  .notEmpty()
  .withMessage("Name cannot be empty")
  .isString()
  .withMessage("Invalid name format");

const email_login = check("email")
  .notEmpty()
  .withMessage("Email cannot be empty")
  .isString()
  .withMessage("Invalid email format")
  .isEmail()
  .withMessage("Invalid email address");

const email_register = check("email")
  .notEmpty()
  .withMessage("Email cannot be empty")
  .isString()
  .withMessage("Invalid email format")
  .isEmail()
  .withMessage("Invalid email address")
  .custom(UserRepository.email_unique);

const password = check("password")
  .notEmpty()
  .withMessage("Password cannot be empty")
  .isString()
  .withMessage("Invalid password format")
  .isLength({ min: 6 })
  .withMessage("Password must be at least 6 characters long");

const register = [name, email_register, password];
const login = [email_login, password];

export default {
  register,
  login,
};
