import { check } from "express-validator";
import { TransactionConstant } from "../../../constant";

const name = check("name")
  .notEmpty()
  .withMessage("Name cannot be empty")
  .isString()
  .withMessage("Invalid name format");

const type = check("type")
  .notEmpty()
  .withMessage("Tipe Transaksi Tidak Boleh Kosong")
  .isIn(TransactionConstant.ALL_TYPE)
  .withMessage("Type Invalid");

const amount = check("amount")
  .notEmpty()
  .withMessage("Amount Tidak Boleh Kosong")
  .isNumeric()
  .withMessage("Amount Harus Berupa Angka");

const date = check("date")
  .notEmpty()
  .withMessage("Tanggal Tidak Boleh Kosong")
  .isISO8601()
  .withMessage("Tanggal Tidak Valid");

const create = [name, type, amount, date];

export default {
  create,
};
