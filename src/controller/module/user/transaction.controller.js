import { ErrorHandler, httpResponse } from "../../../config/http";
import { db, offsetPagination, pagination } from "../../../database";
import { UserTransactionModel } from "../../../database/models";
import { TransactionRepository } from "../../../repositories";
import { getUserByTokenSession } from "../../../utils/jwt";

export default {
  all: async (req, res, next) => {
    try {
      const { page = 1, limit = 10 } = req.query;

      const user = await getUserByTokenSession(req);

      let transactions = await UserTransactionModel.findAndCountAll({
        attributes: ["id", "name", "type", "amount", "date"],
        where: {
          user_id: user.id,
        },
        limit: Number(limit),
        offset: offsetPagination(page, limit),
        order: [["id", "DESC"]],
      });

      transactions = pagination(transactions, page, limit);

      httpResponse(
        res,
        "success",
        "Success! get data transactions",
        transactions
      );
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },

  detail: async (req, res, next) => {
    try {
      const { id } = req.params;

      const user = await getUserByTokenSession(req);

      const transaction = await TransactionRepository.check_transaction(
        id,
        user.id
      );

      httpResponse(
        res,
        "success",
        "Success! get data transactions",
        transaction
      );
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },

  create: async (req, res, next) => {
    try {
      const { type, name, amount, date } = req.body;

      const user = await getUserByTokenSession(req);

      const payload = {
        type,
        name,
        amount,
        date,
        user_id: user.id,
      };

      await UserTransactionModel.create(payload);

      httpResponse(res, "success", "Success! create transaction", null, 201);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },

  update: async (req, res, next) => {
    try {
      const { type, name, amount, date } = req.body;
      const { id } = req.params;
      const user = await getUserByTokenSession(req);

      const transaction = await TransactionRepository.check_transaction(
        id,
        user.id
      );

      const payload = {
        type,
        name,
        amount,
        date,
      };

      await transaction.update(payload);

      httpResponse(res, "success", "Success! update transaction", null, 200);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },

  delete: async (req, res, next) => {
    try {
      const { id } = req.params;
      const user = await getUserByTokenSession(req);

      const transaction = await TransactionRepository.check_transaction(
        id,
        user.id
      );

      await transaction.destroy();

      httpResponse(res, "success", "Success! delete transaction", null, 200);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },

  wallet: async (req, res, next) => {
    try {
      const user = await getUserByTokenSession(req);

      const [result] = await db.query(`
      SELECT 
    COALESCE((SELECT SUM(amount) FROM mono.user_transactions WHERE type = 'INCOME' AND user_id=${user.id} AND deleted_at IS NULL), 0.00) AS income,
    COALESCE((SELECT SUM(amount) FROM mono.user_transactions WHERE type = 'EXPENSE' AND user_id=${user.id} AND deleted_at IS NULL), 0.00) AS expense,
    COALESCE((SELECT SUM(amount) FROM mono.user_transactions WHERE type = 'INCOME' AND user_id=${user.id} AND deleted_at IS NULL), 0.00) -
    COALESCE((SELECT SUM(amount) FROM mono.user_transactions WHERE type = 'EXPENSE' AND user_id=${user.id} AND deleted_at IS NULL), 0.00) AS balance;
      `);

      httpResponse(res, "success", "Success! get wallet", result[0], 200);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },
};
