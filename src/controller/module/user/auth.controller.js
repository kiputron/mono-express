import { ErrorHandler, httpResponse } from "../../../config/http";
import { UserModel } from "../../../database/models";
import { checkHash, hashing } from "../../../utils/hash";
import { generateToken } from "../../../utils/jwt";

export default {
  register: async (req, res, next) => {
    try {
      const { name, email, password } = req.body;

      await UserModel.create({
        name,
        email,
        password: await hashing(password),
      });

      httpResponse(
        res,
        "success",
        "Congratulations! Your registration was successful",
        null
      );
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },

  login: async (req, res, next) => {
    try {
      const { email, password } = req.body;

      const user = await UserModel.scope("with_password").findOne({
        where: { email },
        raw: true,
      });

      if (!user || !checkHash(password, user.password))
        throw new ErrorHandler("Incorrect Email or Password", null, 401);

      delete user.password;

      const token = await generateToken(`${user.id}M0nO`);

      const payload = {
        ...token,
        user,
      };

      httpResponse(
        res,
        "success",
        "Welcome! You have successfully logged in",
        payload
      );
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },
};
