import moment from "moment";

export default {
  default: () => moment().format(),

  with_format: format => moment().format(format),

  log: () => moment().format("DD MMM YYYY HH:mm:ss"),

  validate: (data, format) => moment(data, format, true).isValid(),
};
