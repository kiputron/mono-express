import jwt from "jsonwebtoken";

import { UserModel } from "../database/models";

import { ErrorHandler } from "../config/http";
import env from "../config/env";

const expTime = 7884000000; // Default 3600

async function generateToken(user, payload) {
  try {
    return {
      token: jwt.sign({ user, ...payload }, env.JWT.SECRET_KEY, {
        algorithm: "HS256",
        expiresIn: expTime,
      }),
      expires_in: expTime,
    };
  } catch (err) {
    return Promise.reject(err);
  }
}

async function verifyToken(token) {
  try {
    await jwt.verify(token, env.JWT.SECRET_KEY);

    return true;
  } catch (err) {
    return false;
  }
}

async function getPayloadToken(token) {
  try {
    return await jwt.decode(token);
  } catch (err) {
    return err;
  }
}

async function authenticatedCheck(request) {
  const { authorization } = request.headers;

  if (authorization) {
    const token = authorization.split(" ")[1];

    if (token && (await verifyToken(token))) return true;
  }

  return false;
}

async function JWTCheck(request) {
  try {
    const { authorization } = request.headers;

    if (!authorization) throw new ErrorHandler("Unauthenticated", null, 401);

    const token = authorization.split(" ")[1];

    if (!(await verifyToken(token)))
      throw new ErrorHandler("Unauthenticated", null, 401);

    return token;
  } catch (err) {
    throw new ErrorHandler(err.message, err.data, err.status);
  }
}

async function getUserByTokenSession(req) {
  try {
    const token = await JWTCheck(req);
    const payload = await getPayloadToken(token);

    let index = payload.user.indexOf("M0nO");

    const id = payload.user.substr(0, index);

    const user = await UserModel.findOne({
      where: { id },
      raw: true,
    });

    return user;
  } catch (err) {
    throw new ErrorHandler(err.message, err.data, err.status);
  }
}

export {
  generateToken,
  verifyToken,
  getPayloadToken,
  authenticatedCheck,
  JWTCheck,
  getUserByTokenSession,
};
