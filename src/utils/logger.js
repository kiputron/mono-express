import winston from "winston";

import datetime from "./datetime";

const winston_config = {
  format: winston.format.combine(
    winston.format.colorize({ all: true }),

    winston.format.printf(log => {
      const timestamp = datetime.log();
      const { level } = log;
      let { message } = log;

      const resLog = `${timestamp} [${level}] : `;

      if (log.meta) {
        const { url, from_ip } = log.meta;

        if (url) message = `[${message}] ${log.meta.url}`;
        if (from_ip) message = `${from_ip} | ${message}`;
      }

      return resLog + message;
    })
  ),
};

const logger = winston.createLogger({
  level: "debug",
  transports: [
    new winston.transports.File({
      name: "app-log",
      filename: "./logs/app.log",
      json: false,
      format: winston.format.combine(winston.format.uncolorize()),
    }),

    new winston.transports.File({
      name: "error-log",
      filename: "./logs/error.log",
      level: "error",
      json: false,
      format: winston.format.combine(winston.format.uncolorize()),
    }),

    new winston.transports.Console(),
  ],

  format: winston_config.format,
});

export default logger;
