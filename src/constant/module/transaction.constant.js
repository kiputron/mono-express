const type = {
  EXPENSE: "EXPENSE",
  INCOME: "INCOME",
};

export default {
  TYPE: type,
  ALL_TYPE: Object.keys(type),
};
