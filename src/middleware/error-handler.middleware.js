import logger from "../utils/logger";

const ErrorHandlerMiddleware = (err, req, res, next) => {
  if (res.headerSent) return next(err);

  const message = err.status ? err.message : "Internal Server Error";

  let errors = err.data ? err.data : null;

  if (message === "Invalid data") {
    const arr_check = errors instanceof Array;

    if (!arr_check) errors = [errors];

    const result = [];
    let key = 0;
    const keys = {};

    errors.forEach(val => {
      const { path, msg } = val;

      if (path && msg) {
        if (!keys[path]) {
          keys[path] = 1;
          result[key] = msg;
          key += 1;
        }
      } else {
        result[key] = val;
        key += 1;
      }
    });

    errors = result;
  } else {
    errors = errors instanceof Array ? [errors] : errors;
  }

  logger.warn(message);

  res.status(err.status ? err.status : 500).send({
    meta: {
      status: "failure",
      message,
      code: err.status ? err.status : 500,
    },
    errors,
  });
};

export default ErrorHandlerMiddleware;
