import { urlFormatter } from "../config/http";
import logger from "../utils/logger";

const requestHandler = (req, res, next) => {
  const url = urlFormatter(req);
  const from_ip =
    req.headers["x-forwarded-for"]?.split(",").shift() ||
    req.socket?.remoteAddress;

  logger.info({ message: req.method, meta: { url, from_ip } });

  next();
};

export default requestHandler;
