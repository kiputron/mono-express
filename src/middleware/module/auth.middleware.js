import { ErrorHandler } from "../../config/http";
import { UserModel } from "../../database/models";
import { JWTCheck, getPayloadToken } from "../../utils/jwt";

export default {
  user_guard: async (req, res, next) => {
    try {
      const token = await JWTCheck(req);

      if (!token) throw new ErrorHandler("Unauthenticated", null, 401);

      const payload = await getPayloadToken(token);

      let index = payload.user.indexOf("M0nO");

      const id = payload.user.substr(0, index);

      const user = await UserModel.findOne({
        where: { id },
        raw: true,
      });

      if (!user) throw new ErrorHandler("Unauthenticated", null, 401);

      next();
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status || 500));
    }
  },
};
