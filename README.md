<p align="center">
<img src="./docs/readme/mono.png" width="200" height="200" alt="Mono Image">
</p>
<p align="center">
    <img src="https://img.shields.io/badge/-Backend-blue?style=for-the-badge&logo=">
    <img src="https://img.shields.io/badge/-javascript-yellow?style=for-the-badge&logo=">
</p>

<p align="justify">
This repository is inspired by <a href="https://github.com/alfigufron/Express-MySQL-REST"> Alfi Gufron - Express MySQL REST</a>, in which there are Mono features - Login, Register, Transaction and the main function is Order. I hope this REST API can be useful for you and I invite you to collaborate using this REST API for your needs to build web or mobile applications.
</p>

## Requirements

- **NodeJS** v8+
- **Sequelize** v6+

## Backend Technology Stack

1. **JavaScript**
2. **NodeJS** with **Express.js** framework
3. **REST API**
4. **MySQL** database
5. **Sequelize** ORM

## Get Started

1. Clone repository
2. npm install
3. npm run dev
4. Open Command or Terminal and Write **npm install**
5. Setup ENV File (read env example for setup or run **npm run generate-env**)
6. Next setup db **npm run migrate**
7. Write **npm run dev** in terminal

## API Documentation

1. Copy json file from `docs` to your machine
2. Import Json file to your postman Collections

If you have a problem you can put it in the issue or contact me via the email listed on the profile
